#!/usr/bin/env bats

# Copyright (C) 2018 Aleksander Mistewicz
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

EXISTING_TEST_BE="gentoo-bemerge-test"
NEW_TEST_BE="gentoo-bemerge-test-new-be"
NONEXISTENT_TEST_BE="non-existent-be"

PORTDIR="${PORTDIR:-/usr/portage}"

get_beadm_list() {
    beadm list -sH "${EXISTING_TEST_BE}" | tr -d ";"
}

@test "run 'emerge --version' in the running be" {
    run bemerge -- emerge --version
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Executing the command in the running boot environment." ]
    [[ "${lines[1]}" == "Portage "* ]]
}

@test "run 'ls' in the running be" {
    run bemerge -- ls
    [ "$status" -eq 0 ]
}

@test "run 'emerge --version' in a new (named) be" {
    beadm create "$EXISTING_TEST_BE"

    run bemerge -e "$EXISTING_TEST_BE" -b "$NEW_TEST_BE" -- emerge --version
    [ "$status" -eq 0 ]
    echo "$output"
    [ "${lines[0]}" = "'$NEW_TEST_BE' created successfully." ]
    [[ "${lines[1]}" == "'$NEW_TEST_BE' mounted successfully on: '"*"'." ]]
    [[ "${lines[2]}" == "Portage "* ]]
    [ "${lines[3]}" = "'$NEW_TEST_BE' unmounted successfully." ]
    [ ${#lines[@]} -eq 4 ]

    beadm destroy -F "$NEW_TEST_BE"
    beadm destroy -F "$EXISTING_TEST_BE"
}

@test "run 'emerge --version' in a new (named) be (created from snapshot)" {
    beadm create "$EXISTING_TEST_BE"
    EXISTING_TEST_BE_SNAPSHOT="$EXISTING_TEST_BE@snapshot"
    beadm create "$EXISTING_TEST_BE_SNAPSHOT"
    snapshot_list_before="$(get_beadm_list)"

    run bemerge -e "$EXISTING_TEST_BE_SNAPSHOT" -b "$NEW_TEST_BE" -- emerge --version
    [ "$status" -eq 0 ]
    echo "$output"
    [ "${lines[0]}" = "'$NEW_TEST_BE' created successfully." ]
    [[ "${lines[1]}" == "'$NEW_TEST_BE' mounted successfully on: '"*"'." ]]
    [[ "${lines[2]}" == "Portage "* ]]
    [ "${lines[3]}" = "'$NEW_TEST_BE' unmounted successfully." ]
    [ ${#lines[@]} -eq 4 ]
    snapshot_list_after="$(get_beadm_list)"
    diff <(echo "$snapshot_list_before") <(echo "$snapshot_list_after")

    beadm destroy -F "$NEW_TEST_BE"
    # beadm destroys the snapshot $NEW_TEST_BE was created from - zfs#19
    beadm destroy -F "$EXISTING_TEST_BE"
}

@test "run 'emerge --version' in a new (auto-named) be" {
    beadm create "$EXISTING_TEST_BE"

    run bemerge -e "$EXISTING_TEST_BE" -- emerge --version
    [ "$status" -eq 0 ]
    echo "$output"
    NEW_TEST_BE="$(echo "${lines[1]}" | grep -Eo "'(\w|-)*'" | tr -d "'\n")"
    echo "new be name: '$NEW_TEST_BE'"
    [ "${lines[0]}" = "'$NEW_TEST_BE' created successfully." ]
    [[ "${lines[1]}" == "'$NEW_TEST_BE' mounted successfully on: '"*"'." ]]
    [[ "${lines[2]}" == "Portage "* ]]
    [ "${lines[3]}" = "'$NEW_TEST_BE' unmounted successfully." ]
    [ ${#lines[@]} -eq 4 ]

    beadm destroy -F "$NEW_TEST_BE"
    beadm destroy -F "$EXISTING_TEST_BE"
}

@test "run 'emerge --version' in a new (auto-named) be (created from snapshot)" {
    skip "libbe currently does not support this"

    beadm create "$EXISTING_TEST_BE"
    EXISTING_TEST_BE_SNAPSHOT="$EXISTING_TEST_BE@snapshot"
    beadm create "$EXISTING_TEST_BE_SNAPSHOT"
    snapshot_list_before="$(get_beadm_list)"

    run bemerge -e "$EXISTING_TEST_BE" -- emerge --version
    [ "$status" -eq 0 ]
    echo "$output"
    NEW_TEST_BE="$(echo "${lines[1]}" | grep -Eo "'(\w|-)*'" | tr -d "'\n")"
    echo "new be name: '$NEW_TEST_BE'"
    [ "${lines[0]}" = "'$NEW_TEST_BE' created successfully." ]
    [[ "${lines[1]}" == "'$NEW_TEST_BE' mounted successfully on: '"*"'." ]]
    [[ "${lines[2]}" == "Portage "* ]]
    [ "${lines[3]}" = "'$NEW_TEST_BE' unmounted successfully." ]
    [ ${#lines[@]} -eq 4 ]
    snapshot_list_after="$(get_beadm_list)"
    diff <(echo "$snapshot_list_before") <(echo "$snapshot_list_after")

    beadm destroy -F "$NEW_TEST_BE"
    # beadm destroys the snapshot $NEW_TEST_BE was created from - zfs#19
    beadm destroy -F "$EXISTING_TEST_BE"
}

@test "fail to run 'emerge --version' in a nonexistent be" {
    run bemerge -b "$NONEXISTENT_TEST_BE" -- emerge --version
    [ "$status" -ne 0 ]
    echo "$output"
    [ "${lines[0]}" = "Unable to mount '$NONEXISTENT_TEST_BE': No such BE." ]
    [ "${lines[1]}" = "Mounting '$NONEXISTENT_TEST_BE' failed." ]
    [ ${#lines[@]} -eq 2 ]
}

@test "run glibc pre-merge checks in another be" {
    beadm create "$EXISTING_TEST_BE"

    GLIBC_EBUILD="$PORTDIR/sys-libs/glibc/glibc-9999.ebuild"
    run bemerge -b "$EXISTING_TEST_BE" -- ebuild "$GLIBC_EBUILD" clean setup 2>&1
    [ "$status" -eq 0 ]

    beadm destroy -F "$EXISTING_TEST_BE"
}
