/*
 * Copyright (C) 2017-2018 Aleksander Mistewicz
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file bemerge.c
 * @author Aleksander Mistewicz
 * @brief The main file of bemerge.
 */

/** \cond INCLUDES */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mount.h>
#include <errno.h>
/** \endcond */

/** \cond HIDDEN_SYMBOLS */
#define BE_NAME_ENV_VAR "BE_NAME"

#define SPECIALFS_OPTS (MS_NOSUID | MS_NODEV | MS_NOEXEC | MS_RELATIME)
#define SYSFS_OPTS SPECIALFS_OPTS
#define PROCFS_OPTS SPECIALFS_OPTS
#define DEVFS_OPTS (MS_NOSUID | MS_RELATIME)
#define DEVPTS_OPTS 0

const char *devpts_long_opts = "gid=5,mode=620";

#define PERROR_EXIT(msg) {\
    perror((msg));\
    exit(EXIT_FAILURE);\
}
/** \endcond */

/* from signals.c */
sigset_t *mask_signals();
int unmask_signals(sigset_t *orig_mask);

/* from be.c */
char *create_be(char *orig_be_name, char *new_be_name);
char *mount_be(char *be_name);
int unmount_be(char *be_name);

int run_cmd_in_be(char *be_name, char **argv);
void run_cmd_in_chroot(char *chroot_path, char *be_name, char **argv);
void run_cmd(char **argv);

/**
 * @brief Mountpoints of special filesystems mounted under the chroot directory.
 */
struct special_filesystems {
    char *sysfs_path; /**< sysfs mountpoint; usually "<chroot_path>/sys" */
    char *procfs_path; /**< procfs mountpoint; usually "<chroot_path>/proc" */
    char *devfs_path; /**< udev mountpoint of devtmpfs type;
                        usually "<chroot_path>/dev" */
    char *devpts_path; /**< devpts mountpoint;
                         usually "<chroot_path>/dev/pts" */
};

/**
 * @brief Print usage information for the program.
 */
static void
usage(const char *name)
{
    (void) fprintf(stderr, "usage:\n"
        "\t%s options -- cmd cmd_arguments\n"
        "\n"
        "\toptions:\n"
        "\n"
        "\t-b beName\t\texecutes a command in the given boot environment\n"
        "\t-e beName\t\tcreates a new boot environemnt from a given one\n"
        "\t-e beName@snapshot\tsame as above, but from a snapshot\n",
        name);
}

/**
 * @brief The entry point of bemerge program.
 */
int
main(int argc, char **argv)
{
    int  c;
    char *be_name = NULL;
    char *orig_be_name = NULL;

    if (argc < 2) {
        usage(argv[0]);
        return EXIT_FAILURE;
    }

    while ((c = getopt(argc, argv, "b:e:")) != -1) {
        switch (c) {
        case 'b':
            be_name = optarg;
            break;
        case 'e':
            orig_be_name = optarg;
            break;
        default:
            usage(argv[0]);
            return EXIT_FAILURE;
        }
    }

    if (orig_be_name != NULL &&
            (be_name = create_be(orig_be_name, be_name)) == NULL) {
        fprintf(stderr, "Boot environment creation failed.\n");
        return EXIT_FAILURE;
    }

    if (be_name == NULL) {
        fprintf(stderr, "Executing the command in the running boot environment.\n");
        run_cmd(&argv[optind]);
    }

    return run_cmd_in_be(be_name, &argv[optind]);
}

/**
 * @brief Mount special file systems under the given path.
 *
 * It makes chroot_path suitable for a chroot call.
 *
 * @param chroot_path a base path for file system mountpoints
 * @return special file system paths
 */
struct special_filesystems
mount_special(char *chroot_path) {
    struct special_filesystems result = {0};

    if (asprintf(&result.sysfs_path, "%s/%s", chroot_path, "sys") == -1)
        PERROR_EXIT("sysfs path preparation failed");

    if (asprintf(&result.procfs_path, "%s/%s", chroot_path, "proc") == -1)
        PERROR_EXIT("proc path preparation failed");

    if (asprintf(&result.devfs_path, "%s/%s", chroot_path, "dev") == -1)
        PERROR_EXIT("dev path preparation failed");

    if (asprintf(&result.devpts_path, "%s/%s", result.devfs_path, "pts") == -1)
        PERROR_EXIT("devpts path preparation failed");

    if (mount("sysfs", result.sysfs_path, "sysfs", SYSFS_OPTS, NULL) != 0)
        PERROR_EXIT("mounting sysfs failed");

    if (mount("proc", result.procfs_path, "proc", PROCFS_OPTS, NULL) != 0) {
        int saved_errno = errno;
        umount(result.sysfs_path);
        errno = saved_errno;
        PERROR_EXIT("mounting proc failed");
    }

    if (mount("udev", result.devfs_path, "devtmpfs", DEVFS_OPTS, NULL) != 0) {
        int saved_errno = errno;
        umount(result.sysfs_path);
        umount(result.procfs_path);
        errno = saved_errno;
        PERROR_EXIT("mounting dev failed");
    }

    if (mount("devpts", result.devpts_path, "devpts", DEVPTS_OPTS,
        devpts_long_opts) != 0) {
        int saved_errno = errno;
        umount(result.sysfs_path);
        umount(result.procfs_path);
        umount(result.devfs_path);
        errno = saved_errno;
        PERROR_EXIT("mounting devpts failed");
    }

    return result;
}

/**
 * @brief Free the memory allocated for the structure members.
 */
void
free_special_filesystems(struct special_filesystems special_fs) {
    free(special_fs.sysfs_path);
    free(special_fs.procfs_path);
    free(special_fs.devfs_path);
    free(special_fs.devpts_path);
}

/**
 * @brief Save errno, unmount the boot environment, and exit the program.
 *
 * @param be_name name of the boot environment to unmount
 * @param msg message to add as a context to errno
 */
void
unmount_be_after_error(char *be_name, const char *msg) {
    int saved_errno = errno;
    unmount_be(be_name);
    errno = saved_errno;
    PERROR_EXIT(msg);
}

/**
 * @brief Run the command with its arguments in the named boot environment.
 *
 * @param be_name name of the boot environment used for execution
 * @param argv command line to execute in the boot environment, where argv[0]
 * is a command name present in PATH (after chroot)
 */
int
run_cmd_in_be(char *be_name, char **argv) {
    char        *chroot_path = NULL;
    pid_t       cmd;
    int         cmd_exit_status;
    sigset_t    *orig_mask;

    if ((chroot_path = mount_be(be_name)) == NULL) {
        fprintf(stderr, "Mounting '%s' failed.\n", be_name);
        exit(EXIT_FAILURE);
    }

    free_special_filesystems(mount_special(chroot_path));

    if ((cmd = fork()) == -1)
        unmount_be_after_error(be_name, "fork failed");

    if (cmd == 0)
        run_cmd_in_chroot(chroot_path, be_name, argv);

    if ((orig_mask = mask_signals()) == NULL)
        unmount_be_after_error(be_name,
                "masking signals for parent process failed");

    if (waitpid(cmd, &cmd_exit_status, 0) == -1)
        unmount_be_after_error(be_name, "waiting for child failed");

    if (!WIFEXITED(cmd_exit_status))
        unmount_be_after_error(be_name, "child did not exit");

    if (unmount_be(be_name) != 0) {
        fprintf(stderr, "Unmounting '%s' failed.\n", be_name);
        exit(EXIT_FAILURE);
    }

    if (rmdir(chroot_path) == -1)
        PERROR_EXIT("removal of temporary directory failed");

    if (unmask_signals(orig_mask) != 0)
        PERROR_EXIT("unmasking signals for parent process failed");

    return WEXITSTATUS(cmd_exit_status);
}

/**
 * @brief Change root, set BE_NAME_ENV_VAR, and execute the given command.
 *
 * @param chroot_path path to chroot into
 * @param be_name value for BE_NAME_ENV_VAR environment variable
 * @param argv command line to execute in the boot environment, where argv[0]
 * is a command name present in PATH (after chroot)
 */
void
run_cmd_in_chroot(char *chroot_path, char *be_name, char **argv) {
    if (setenv(BE_NAME_ENV_VAR, be_name, 1) != 0)
        PERROR_EXIT("setenv failed");

    char *cwd;
    if ((cwd = getcwd(NULL, 0)) == NULL)
        PERROR_EXIT("getcwd failed");

    if (chroot(chroot_path) != 0)
        PERROR_EXIT("chroot failed");

    if (chdir(cwd) != 0) {
        free(cwd);
        PERROR_EXIT("chdir failed");
    }
    free(cwd);

    run_cmd(argv);
}

/**
 * @brief Execute the given command.
 */
void
run_cmd(char **argv) {
    execvp(argv[0], argv);
    PERROR_EXIT("exec failed");
}
