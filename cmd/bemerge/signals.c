/*
 * Copyright (C) 2017 Aleksander Mistewicz
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file signals.c
 * @author Aleksander Mistewicz
 * @brief File providing functions manipulating signal masks.
 */

/** \cond INCLUDES */
#include <stdlib.h>
#include <signal.h>
/** \endcond */

/**
 * @brief Mask SIGINT, SIGTSTP, SIGCONT signals.
 *
 * It creates a mask and applies it.
 *
 * @return the mask that was active before the call to this function
 */
sigset_t *
mask_signals() {
    sigset_t mask;
    sigset_t *orig_mask = malloc(sizeof(*orig_mask));

    if (orig_mask == NULL)
        return NULL;

    if ((sigemptyset (&mask) != 0) ||
            (sigaddset (&mask, SIGINT) != 0) ||
            (sigaddset (&mask, SIGTSTP) != 0) ||
            (sigaddset (&mask, SIGCONT) != 0))
        goto out;

    if (sigprocmask(SIG_BLOCK, &mask, orig_mask) != 0)
        goto out;

    return orig_mask;
out:
    free(orig_mask);
    return NULL;
}

/**
 * @brief Change the signal mask and free the allocated memory.
 *
 * @param orig_mask pointer to the mask that should be applied
 */
int
unmask_signals(sigset_t *orig_mask) {
    if (sigprocmask(SIG_SETMASK, orig_mask, NULL) != 0)
        goto out;

    free(orig_mask);
    return 0;
out:
    free(orig_mask);
    return -1;
}
