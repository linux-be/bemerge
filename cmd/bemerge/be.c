/*
 * Copyright (C) 2017-2018 Aleksander Mistewicz
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file be.c
 * @author Aleksander Mistewicz
 * @brief File providing wrappers for libbe functions.
 */

/** \cond INCLUDES */
#include <string.h>
/** \endcond */

#include <libbe.h>
#include <sys/nvpair.h>

/**
 * @brief Extract the automatically generated name of the new boot environment.
 *
 * It is a helper function of create_be.
 *
 * @param be_attrs the argument that was given to be_copy call
 * @return the name of the new boot environment
 */
char *
get_be_name_from_nvlist(nvlist_t *be_attrs) {
    char    *new_be_name;
    char    *tmp_new_be_name;

    if (nvlist_lookup_string(be_attrs,
                BE_ATTR_NEW_BE_NAME, &tmp_new_be_name) != 0) {
        fprintf(stderr, "Failed to get '%s' attribute.\n", BE_ATTR_NEW_BE_NAME);
        return NULL;
    }

    // FIXME new_be_name is never freed in this case.
    if ((new_be_name = strdup(tmp_new_be_name)) == NULL)
        fprintf(stderr, "Failed to duplicate string.\n");

    return new_be_name;
}

/**
 * @brief Create a new boot environment.
 *
 * If orig_be_name is a snapshot, it will be used to create
 * the new boot environment. Otherwise, a new snapshot will be created.
 * If new_be_name is NULL, an automatically generated name will be used.
 *
 * @param orig_be_name name of the boot environment the new boot environment
 * will be created from.
 * @param new_be_name name of the new boot environment
 * @return the name of the new boot environment
 */
char *
create_be(char *orig_be_name, char *new_be_name)
{
    nvlist_t    *be_attrs;
    int         err;
    char        *snap_name = NULL;

    if ((snap_name = strrchr(orig_be_name, '@')) != NULL) {
        if (snap_name[1] == '\0') {
            fprintf(stderr, "The name of the snapshot is missing in '%s'.\n",
                    orig_be_name);
            return NULL;
        } else {
            snap_name[0] = '\0';
            snap_name++;
        }
    }

    if (nvlist_alloc(&be_attrs, NV_UNIQUE_NAME, 0) != 0)
        return NULL;

    if (nvlist_add_string(be_attrs, BE_ATTR_ORIG_BE_NAME, orig_be_name) != 0)
        goto out;

    if (snap_name != NULL && nvlist_add_string(be_attrs,
        BE_ATTR_SNAP_NAME, snap_name) != 0)
        goto out;

    if (new_be_name != NULL && nvlist_add_string(be_attrs,
        BE_ATTR_NEW_BE_NAME, new_be_name) != 0)
        goto out;

    err = be_copy(be_attrs);

    switch (err) {
    case BE_SUCCESS:
        if (new_be_name == NULL &&
                (new_be_name = get_be_name_from_nvlist(be_attrs)) == NULL)
            goto out;
        fprintf(stderr, "'%s' created successfully.\n", new_be_name);
        break;
    default:
        fprintf(stderr, "Unable to create '%s': %s\n",
                new_be_name, be_err_to_str(err));
        goto out;
    }

    nvlist_free(be_attrs);
    return new_be_name;
out:
    nvlist_free(be_attrs);
    return NULL;
}

/**
 * @brief Mount the named boot environment.
 *
 * A temporary directory, created with a call to mkdtemp, is used for
 * the mountpoint.
 *
 * @param be_name name of the boot environment to mount.
 * @return the mountpoint path
 */
char *
mount_be(char *be_name) {
    int         err;
    nvlist_t    *be_attrs;
    char        *mountpoint;
    int         mount_flags = BE_MOUNT_FLAG_SHARED_FS;
    const char  *tmpdir = getenv("TMPDIR");
    const char  *tmpname = "bemerge.XXXXXX";
    char        *tmp_mp;

    if (tmpdir == NULL)
        tmpdir = "/tmp";

    if (asprintf(&tmp_mp, "%s/%s", tmpdir, tmpname) == -1)
        return NULL;

    mountpoint = mkdtemp(tmp_mp);

    if (nvlist_alloc(&be_attrs, NV_UNIQUE_NAME, 0) != 0)
        return NULL;

    if (nvlist_add_string(be_attrs, BE_ATTR_ORIG_BE_NAME, be_name) != 0)
        goto out;

    if (nvlist_add_string(be_attrs, BE_ATTR_MOUNTPOINT, mountpoint) != 0)
        goto out;

    if (nvlist_add_uint16(be_attrs, BE_ATTR_MOUNT_FLAGS, mount_flags) != 0)
        goto out;

    err = be_mount(be_attrs);

    switch (err) {
    case BE_SUCCESS:
        fprintf(stderr, "'%s' mounted successfully on: '%s'.\n", be_name, mountpoint);
        break;
    default:
        fprintf(stderr, "Unable to mount '%s': %s\n",
                be_name, be_err_to_str(err));
        goto out;
    }

    nvlist_free(be_attrs);
    return mountpoint;
out:
    nvlist_free(be_attrs);
    return NULL;
}

/**
 * @brief Unmount the named boot environment.
 *
 * All descendant mountpoints are automatically unmounted.
 *
 * @param be_name name of the boot environment to unmount
 */
int
unmount_be(char *be_name) {
    nvlist_t    *be_attrs;
    int         err;
    int         unmount_flags = 0;

    if (nvlist_alloc(&be_attrs, NV_UNIQUE_NAME, 0) != 0)
        return 1;

    if (nvlist_add_string(be_attrs, BE_ATTR_ORIG_BE_NAME, be_name) != 0)
        goto out;

    if (nvlist_add_uint16(be_attrs, BE_ATTR_UNMOUNT_FLAGS, unmount_flags) != 0)
        goto out;

    err = be_unmount(be_attrs);

    switch (err) {
    case BE_SUCCESS:
        fprintf(stderr, "'%s' unmounted successfully.\n", be_name);
        break;
    default:
        fprintf(stderr, "Unable to unmount '%s': %s\n",
                be_name, be_err_to_str(err));
        goto out;
    }

    nvlist_free(be_attrs);
    return 0;
out:
    nvlist_free(be_attrs);
    return 1;
}

