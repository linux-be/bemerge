all: bemerge

-include localdefs.mk

CFLAGS += -std=c99 -I/usr/include/libzfs/ -I/usr/include/libspl/ -D _GNU_SOURCE -Wall

ifdef DEBUG
	CFLAGS += -O1 -g3 -D _DEBUG
else
	CFLAGS += -O2
endif

OBJS := cmd/bemerge/bemerge.o cmd/bemerge/signals.o cmd/bemerge/be.o

bemerge: $(OBJS) $(COBJS)
	gcc $(OBJS) $(COBJS) -lnvpair -lbe -lzfs -o $@

doc:
	doxygen doxygen.conf

-include *.d

%.o: %.c
	gcc $(CFLAGS) -c -o $@ $<
	gcc -MM $< > $*.d

clean:
	rm -rvf $(OBJS) cmd/bemerge/*.d bemerge man/
